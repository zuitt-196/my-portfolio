import project1 from '../assets/projects/andress.png';
import project2 from '../assets/projects/food-website.png';
import project3 from '../assets/projects/inventory.png';
import project4 from '../assets/projects/tesla-website.png';
import project5 from '../assets/projects/E-commers.png';




export const data=[
    {
        id:1,
        name:"Andress Soraino College website Application (HTML,CSS,Js)",
        image:project1,
        github:"https://github.com/vhongbercasio/Front-End-project-1.io",
        live:"https://luminous-puffpuff-5432b4.netlify.app/index.html",
    },
    {
        id:2,
        name:"Foods Website Application (HTML,CSS,React.js)",
        image:project2,
        github:"https://gitlab.com/zuitt-196/food-app-react-js",
        live:"https://vercel.com/matervhong/food-app-react-js-ln4d",
    },
    {
        id:3,
        name:"Inventory Management Website (mongoDb,React.js, Express.js Node.js)",
        image:project3,
        github:"https://gitlab.com/zuitt-196/siltoninvent-app-mern",
        live:"https://siltoninvent-app.vercel.app/",
    },
    {
        id:4,
        name:"Tesla Clone Website (HTML,CSS,React,js)",
        image:project4,
        github:"https://gitlab.com/zuitt-196/tesla-clone-website",
        live:"https://tesla-clone-website-6zma1aagn-matervhong.vercel.app/",
    },
    {
        id:5,
        name:"E-commerse (mongoDb,React.js, Express.js Node.js)",
        image:project5,
        github:"",
        live:"",
    },


]