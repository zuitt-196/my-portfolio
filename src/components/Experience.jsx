import React from "react";
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import SchoolIcon from "@material-ui/icons/School";
import WorkIcon from "@material-ui/icons/Work";

function Experience() {
  return (
    <div className="  bg-[#d4deee] ">
      <VerticalTimeline lineColor="#3e497a" className="mb-8">
        <VerticalTimelineElement
          className="mt-8"
          date="2015-2016"
          iconStyle={{ background: "#3e497a", color: "#fff" }}
          icon={<SchoolIcon />}
        >
          <h3 className="vertical-timeline-element-title">
                Highschool
          </h3>
              <p>San Isidro National HighSchool</p>
              <ul>
                  <li>Loyalty Awarded</li>
                  <li>Special Awarded</li>
              </ul>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className="vertical-timeline-element--education"
          date="2016-2018"
          iconStyle={{ background: "#3e497a", color: "#fff" }}
          icon={<SchoolIcon />}
        >
          <h3 className="vertical-timeline-element-title">
                Senior High School 
          </h3>
              <p>Computer System Servicing</p>
              <p>Saint Vincent Diocesan College</p>
              <ul>
                  <li>NC2 Awarded</li>
          
              </ul>

        </VerticalTimelineElement>
        <VerticalTimelineElement
          className="vertical-timeline-element--work"
          date="2018-2021"
          iconStyle={{ background: "#3e497a", color: "#fff" }}
          icon={<SchoolIcon />}
        >
          <h3 className="vertical-timeline-element-title">
                College
          </h3>
              <p>Bachelor of Science in Information Technology</p>
              <p>Andress Soriano collge of bislig</p>
              <ul>
             
                  <li>Certification Recognation Awarded</li>
          
              </ul>

        </VerticalTimelineElement>
        <VerticalTimelineElement
          className="vertical-timeline-element--work"
          date="2021-2022 "
          iconStyle={{ background: "#3e497a", color: "#fff" }}
          icon={<SchoolIcon />}
        >
                        <h3 className="vertical-timeline-element-title">
                  Techinal Education And Skills Development or TESDA 
          </h3>
                    <ul>
                  <li>Web Development NC3</li>
                  <li>Computer System Awarded NC2</li>
          
                    </ul>
          
        </VerticalTimelineElement>



        <VerticalTimelineElement
          className="vertical-timeline-element--work"
          date="2022"
          iconStyle={{ background: "#3e497a", color: "#fff" }}
          icon={<SchoolIcon />}
        >
                        <h3 className="vertical-timeline-element-title">
                  Zuitt Bootcamp
          </h3>
          <p>Postgraduate/Certificate Course, Web Development</p>

                    <ul>
  
                     <li>Certificate of Completion </li>
          
                    </ul>
          
        </VerticalTimelineElement>
      </VerticalTimeline>

      
      
    </div>
  );
}

export default Experience;
